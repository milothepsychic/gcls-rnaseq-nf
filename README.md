# Google Cloud Lifesciences RNAseq-NF pipeline 

A basic pipeline for quantification of genomic features from short read data
implemented with Nextflow on the Google Cloud Lifesciences platform.

[![nextflow](https://img.shields.io/badge/nextflow-%E2%89%A50.24.0-brightgreen.svg)](http://nextflow.io)
[![Build Status](https://travis-ci.org/nextflow-io/rnaseq-nf.svg?branch=master)](https://travis-ci.org/nextflow-io/rnaseq-nf)

## Requirements 

* Unix-like operating system (Linux, macOS, etc)
* Java 8 

## Quickstart 

1. If you don't have it already install Docker in your computer. Read more [here](https://docs.docker.com/).

2. Install Nextflow (version 0.24.x or higher):
      
        curl -s https://get.nextflow.io | bash

3. Set environmental variables:

        export NXF_MODE=google
        export GOOGLE_APPLICATION_CREDENTIALS={path_to_your_storage_credentials_key}.json

3. Launch the pipeline execution: 

        nextflow -C ./nextflow.config run main.nf -profile gcp -w gs://{your_bucket_name}/workdir
        
Note: the very first time you execute it, it will take a few minutes to download the pipeline 
from this GitHub repository and the the associated Docker images needed to execute the pipeline.  

## Components 

GCLS-RNASeq-NF uses the following software components and tools: 

* [Salmon](https://combine-lab.github.io/salmon/) 0.14.2
* [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) 0.11.8
* [BBMap.sh](https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/bbmap-guide/) 38.69
* [Multiqc](https://multiqc.info) 1.7

